# TLDR

This is a simple command line program to create proxy decks


```
    proxyprint decklist.txt
```
This will create a "decklist" folder in your current directory
and dump the jpeg images. There are a bunch of options
exposed as command line options you can tweak. List
them and get descriptions with:

```
    proxyprint --help
```

## Overview:

Basically just a wrapper around:
- mtgtools (and scryfall via this)
    - https://pypi.org/project/mtgtools/
    - https://scryfall.com/
- tested with copy pasted decklists from mtgtop8.com no idea if they'll work with anything else
    - copy the decklist part of the page
    - open Notepad or equivalent raw text editor
    - save file as deckname.txt
    - `proxyprint deckname.txt`


## Release Notes

1.0.0 - initial release
1.0.1 - fix minor bug and expose options
1.0.2 - update readme and fix dependency install bug

## Example
Below is an example file that will work with proxy printer. You could copy paste it and save it as deathtaxes.txt or something to test this out

```
24 LANDS
3 Karakas
7 Plains
4 Rishadan Port
6 Snow-Covered Plains
4 Wasteland
25 CREATURES
4 Flickerwisp
2 Mirran Crusader
4 Mother of Runes
1 Palace Jailer
2 Phyrexian Revoker
	
2 Recruiter of the Guard
1 Sanctum Prelate
4 Stoneforge Mystic
4 Thalia, Guardian of Thraben
1 Walking Ballista
4 INSTANTS and SORC.
4 Swords to Plowshares
7 OTHER SPELLS
4 Aether Vial
1 Batterskull
1 Sword of Fire and Ice
1 Umezawa's Jitte
	
SIDEBOARD
1 Cataclysm
1 Containment Priest
2 Council's Judgment
1 Disenchant
1 Ethersworn Canonist
1 Faerie Macabre
1 Gideon, Ally of Zendikar
1 Leonin Relic-Warder
1 Nahiri, the Lithomancer
1 Path to Exile
1 Recruiter of the Guard
1 Rest in Peace
2 Surgical Extraction
```